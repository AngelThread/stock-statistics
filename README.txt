
Assumptions
 -  It has been assumed that the business requires top selling three products as the products with highest availability.

Information
Application uses embedded H2 database and there is online client exists when the application is running.
http://localhost:8080/h2-console/login.jsp?jsessionid=e89de761616d22af489c0139b2b7d5d6

username: root
password:root

How to run the application
1. mvn spring-boot:run in project directory
2. Execute man clean install and then execute  java -jar target/stock-statistics-0.0.1-SNAPSHOT.jar
