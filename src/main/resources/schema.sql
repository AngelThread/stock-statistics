CREATE TABLE IF NOT EXISTS `stocks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(255) NOT NULL,
    `timestamp` timestamp,
  `product_id` varchar(255) NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
);
CREATE TABLE IF NOT EXISTS `sold_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
   `product_id` varchar(255) NOT NULL,
  `items_sold` int(11) unsigned NOT NULL,
   `timestamp` timestamp,
  PRIMARY KEY (`id`)
);

