package com.stock.statistics.stockstatistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockStatisticsApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockStatisticsApplication.class, args);
	}

}
