package com.stock.statistics.stockstatistics.service.impl;

import com.stock.statistics.stockstatistics.controller.exception.NotUniqueStockIdException;
import com.stock.statistics.stockstatistics.controller.exception.OutdatedStockException;
import com.stock.statistics.stockstatistics.entity.SoldProduct;
import com.stock.statistics.stockstatistics.entity.Stock;
import com.stock.statistics.stockstatistics.modal.StockData;
import com.stock.statistics.stockstatistics.modal.StockDataUtil;
import com.stock.statistics.stockstatistics.repository.SoldProductRepository;
import com.stock.statistics.stockstatistics.repository.StockRepository;
import com.stock.statistics.stockstatistics.service.StockService;
import java.util.Optional;
import java.util.OptionalInt;
import javax.transaction.Transactional;
import lombok.NonNull;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
public class StockServiceImpl implements StockService {

  private final StockRepository stockRepository;
  private final SoldProductRepository soldProductRepository;

  public StockServiceImpl(
      StockRepository stockRepository,
      SoldProductRepository soldProductRepository) {
    this.stockRepository = stockRepository;
    this.soldProductRepository = soldProductRepository;
  }

  @Override
  @Transactional
  public StockData save(@NonNull StockData stockData) {
    Optional<Stock> existingStockInfo = stockRepository
        .findUpToDateRequestInfo(stockData.getProductId());

    OptionalInt existingProductQuantity = checkExistingStockInfoAndGetQuantity(stockData,
        existingStockInfo);

    Stock stock = StockDataUtil.toStock(stockData, existingStockInfo);
    stockRepository.save(stock);

    if (existingProductQuantity.isPresent() && isProductSold(existingProductQuantity, stock)) {
      findOutSoldQuantityAndPersist(existingProductQuantity.getAsInt(), stock);
    }

    return stockData;
  }

  private OptionalInt checkExistingStockInfoAndGetQuantity(@NonNull StockData stockData,
      Optional<Stock> stockWithProductIdAndMaxTimestamp) {
    OptionalInt existingProductQuantity = OptionalInt.empty();

    if (stockWithProductIdAndMaxTimestamp.isPresent()) {
      Stock existingStock = stockWithProductIdAndMaxTimestamp.get();
      checkFreshnessOfStockInfo(stockData, existingStock);
      existingProductQuantity = OptionalInt
          .of(stockWithProductIdAndMaxTimestamp.get().getQuantity());
    }
    return existingProductQuantity;
  }

  private void checkFreshnessOfStockInfo(@NonNull StockData stockData, Stock existingStock) {
    if (!existingStock.getTimestamp()
        .isBefore(stockData.getTimestamp())) {
      throw new OutdatedStockException(
          "The system has fresher stock information with the same timestamp or newer!");
    }
  }

  private void findOutSoldQuantityAndPersist(int existingProductQuantity, Stock stock) {

    int newSoldQuantity = existingProductQuantity - stock
        .getQuantity();
    persistSoldProductInfo(stock, newSoldQuantity);
  }

  private void persistSoldProductInfo(Stock stock,
      int totalSoldQuantity) {
    SoldProduct upToDateSoldProductInfo = prepareSoldProductInfo(stock,
        totalSoldQuantity);

    soldProductRepository.save(upToDateSoldProductInfo);
  }

  private SoldProduct prepareSoldProductInfo(Stock stock,
      int totalSoldQuantity) {
    SoldProduct upToDateSoldProductInfo = new SoldProduct();
    upToDateSoldProductInfo.setItemsSold(totalSoldQuantity);
    upToDateSoldProductInfo.setProductId(stock.getProductId());
    upToDateSoldProductInfo.setTimestamp(stock.getTimestamp());

    return upToDateSoldProductInfo;
  }

  private boolean isProductSold(OptionalInt existingProductQuantity, Stock stock) {
    return existingProductQuantity.getAsInt() > stock
        .getQuantity();
  }

  @Override
  public Optional<Stock> findStockByProductId(String productId) {
    return stockRepository
        .findUpToDateRequestInfo(productId);
  }


}
