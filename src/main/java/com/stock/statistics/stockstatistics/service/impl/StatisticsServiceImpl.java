package com.stock.statistics.stockstatistics.service.impl;

import com.stock.statistics.stockstatistics.controller.dto.Range;
import com.stock.statistics.stockstatistics.entity.SoldProduct;
import com.stock.statistics.stockstatistics.entity.Stock;
import com.stock.statistics.stockstatistics.modal.StockStatistics;
import com.stock.statistics.stockstatistics.repository.SoldProductRepository;
import com.stock.statistics.stockstatistics.repository.StockRepository;
import com.stock.statistics.stockstatistics.service.StatisticsService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class StatisticsServiceImpl implements StatisticsService {

  private final SoldProductRepository soldProductRepository;
  private final StockRepository stockRepository;

  public StatisticsServiceImpl(
      SoldProductRepository soldProductRepository,
      StockRepository stockRepository) {
    this.soldProductRepository = soldProductRepository;
    this.stockRepository = stockRepository;
  }

  @Override
  public StockStatistics getStockStatistics(Range range) {
    Optional<List<SoldProduct>> topSellingProducts = getTopSellingProducts(range);

    Optional<List<Stock>> productsWithTopThreeQuantity = stockRepository
        .findProductsWithTopThreeQuantity(range.getTimestamp());

    return StockStatistics.builder()
        .topAvailableProducts(productsWithTopThreeQuantity.orElse(new ArrayList<Stock>()))
        .topSellingProducts(topSellingProducts.orElse(new ArrayList<SoldProduct>())).build();
  }

  private Optional<List<SoldProduct>> getTopSellingProducts(Range range) {
    return soldProductRepository
        .findTopSellingThreeProductsAfterGivenTime(range.getTimestamp());

  }
}
