package com.stock.statistics.stockstatistics.service;

import com.stock.statistics.stockstatistics.controller.dto.Range;
import com.stock.statistics.stockstatistics.modal.StockStatistics;

public interface StatisticsService {

  StockStatistics getStockStatistics(Range range);
}
