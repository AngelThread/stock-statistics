package com.stock.statistics.stockstatistics.service;

import com.stock.statistics.stockstatistics.entity.Stock;
import com.stock.statistics.stockstatistics.modal.StockData;
import java.util.Optional;
import lombok.NonNull;

public interface StockService {

  StockData save(@NonNull StockData stockData);

  Optional<Stock> findStockByProductId(String productId);
}
