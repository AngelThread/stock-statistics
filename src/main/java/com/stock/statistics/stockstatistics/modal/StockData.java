package com.stock.statistics.stockstatistics.modal;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class StockData {

  private String id;
  private LocalDateTime timestamp;
  private String productId;
  private int quantity;
}
