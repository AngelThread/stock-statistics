package com.stock.statistics.stockstatistics.modal;

import com.stock.statistics.stockstatistics.controller.dto.Range;
import com.stock.statistics.stockstatistics.entity.SoldProduct;
import com.stock.statistics.stockstatistics.entity.Stock;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class StockStatistics {

  private LocalDateTime requestTimestamp;
  private Range range;
  private List<Stock> topAvailableProducts;
  private List<SoldProduct> topSellingProducts;


}
