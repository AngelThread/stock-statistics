package com.stock.statistics.stockstatistics.modal;

import com.stock.statistics.stockstatistics.controller.dto.StockDto;
import com.stock.statistics.stockstatistics.entity.Stock;
import java.util.Optional;
import lombok.NonNull;

public class StockDataUtil {


  public static StockData toStockData(@NonNull StockDto stockDto) {

    return StockData.builder().id(stockDto.getId()).timestamp(stockDto.getTimestamp())
        .productId(stockDto.getProductId()).quantity(stockDto.getQuantity()).build();
  }

  public static StockDto toStockDto(@NonNull StockData stockDto) {

    return StockDto.builder().id(stockDto.getId()).timestamp(stockDto.getTimestamp())
        .productId(stockDto.getProductId()).quantity(stockDto.getQuantity()).build();
  }

  public static Stock toStock(@NonNull StockData stockData,
      Optional<Stock> existingStockInfo) {
    Stock stock = existingStockInfo.orElse(new Stock());
    stock.setProductId(stockData.getProductId());
    stock.setQuantity(stockData.getQuantity());
    stock.setTimestamp(stockData.getTimestamp());
    stock.setStockId(stockData.getId()); // stock data carries stock id in id field

    return stock;
  }

  public static StockData fromStocktoStockData(@NonNull Stock stock) {

    return StockData.builder().timestamp(stock.getTimestamp()).id(stock.getStockId())
        .quantity(stock.getQuantity()).productId(stock.getProductId()).build();

  }
}
