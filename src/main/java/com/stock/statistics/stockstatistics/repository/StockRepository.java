package com.stock.statistics.stockstatistics.repository;

import com.stock.statistics.stockstatistics.entity.Stock;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * This repository operators on stock of products
 */

@Repository
public interface StockRepository extends CrudRepository<Stock, Integer> {

  @Query(nativeQuery = true, value = "SELECT s.* FROM STOCKS s where s.product_id=:productId")
  Optional<Stock> findUpToDateRequestInfo(@Param("productId") String productId);

  @Query(nativeQuery = true, value = "SELECT * FROM STOCKS s where TRUNC(s.timestamp) >=:date order by s.quantity desc limit 3")
  Optional<List<Stock>> findProductsWithTopThreeQuantity(@Param("date") LocalDate date);

}