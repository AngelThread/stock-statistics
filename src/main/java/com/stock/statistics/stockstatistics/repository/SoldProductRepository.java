package com.stock.statistics.stockstatistics.repository;

import com.stock.statistics.stockstatistics.entity.SoldProduct;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * This repository operators on statistics of sold products
 */

@Repository
public interface SoldProductRepository extends CrudRepository<SoldProduct, Integer> {

  /**
   * It has been assumed that the business requires top selling three products as the products with
   * highest availability
   */
  @Query(nativeQuery = true, value = "SELECT s.* FROM sold_products s where TRUNC(s.timestamp) >=:date order by s.items_sold desc limit 3")
  Optional<List<SoldProduct>> findTopSellingThreeProductsAfterGivenTime(@Param("date") LocalDate date);
}
