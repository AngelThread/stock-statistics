package com.stock.statistics.stockstatistics.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "sold_products", uniqueConstraints = @UniqueConstraint(columnNames = "product_id"), indexes = {
    @Index(columnList = "items_sold,timestamp")})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoldProduct {

  @Id
  @GeneratedValue
  private int id;
  @Column(name = "product_id")
  private String productId;
  @Column(name = "items_sold")
  private int itemsSold;
  @Column(name = "timestamp")
  private LocalDateTime timestamp;

  @Version
  private int version;
}
