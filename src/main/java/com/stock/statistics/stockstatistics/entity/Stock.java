package com.stock.statistics.stockstatistics.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "stocks", uniqueConstraints = {@UniqueConstraint(columnNames = "stock_id"),
    @UniqueConstraint(columnNames = "product_id")}, indexes = {
    @Index(columnList = "quantity")})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Stock {

  @Id
  @GeneratedValue
  private int id;
  @Column(name = "stock_id")
  private String stockId;
  @Column(name = "timestamp")
  private LocalDateTime timestamp;
  @Column(name = "product_id")
  private String productId;
  private int quantity;

  @Version
  private int version;

}
