package com.stock.statistics.stockstatistics.controller.exception;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class ErrorMessage {

  private List<String> errors;
  private int status;
}
