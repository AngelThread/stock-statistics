package com.stock.statistics.stockstatistics.controller.exception;

public class OutdatedStockException extends RuntimeException {

  public OutdatedStockException(String message) {
    super(message);
  }
}
