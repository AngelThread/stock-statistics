package com.stock.statistics.stockstatistics.controller.exception;

public class NotUniqueStockIdException extends RuntimeException {

  public NotUniqueStockIdException(String message) {
    super(message);
  }
}
