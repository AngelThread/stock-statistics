package com.stock.statistics.stockstatistics.controller;

public class EndPoints {

  public static final String UPDATE_STOCK = "/updateStock";
  public static final String STOCK = "/stock";
  public static final String STATISTICS = "/statistics";

}