package com.stock.statistics.stockstatistics.controller.dto;

import java.time.LocalDate;

public enum Range {
  today("today", LocalDate.now()), lastMonth("lastMonth",
      LocalDate.now().minusMonths(1));
  private String name;
  private LocalDate timestamp;

  private Range(String name, LocalDate localDate) {
    this.name = name;
    this.timestamp = localDate;
  }

  public String getName() {
    return name;
  }

  public LocalDate getTimestamp() {
    return timestamp;
  }
}
