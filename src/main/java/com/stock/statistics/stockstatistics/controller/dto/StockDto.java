package com.stock.statistics.stockstatistics.controller.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.stock.statistics.stockstatistics.controller.dto.StockDto.StockDtoBuilder;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import javax.validation.constraints.NotEmpty;
import lombok.Builder;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

@Builder
@Value
@JsonDeserialize(builder = StockDtoBuilder.class)
public class StockDto {

  @NotEmpty
  private String id;
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  private LocalDateTime timestamp;
  @NotEmpty
  private String productId;
  private int quantity;
  @JsonPOJOBuilder(withPrefix = "")
  public static class StockDtoBuilder {

  }
}
