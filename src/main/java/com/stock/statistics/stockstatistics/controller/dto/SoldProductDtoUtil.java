package com.stock.statistics.stockstatistics.controller.dto;

import com.stock.statistics.stockstatistics.entity.SoldProduct;
import java.util.Objects;

public class SoldProductDtoUtil {

  public static SoldProductDto toSoldProductDto(SoldProduct soldProduct) {
    if (Objects.isNull(soldProduct)) {
      return null;
    }

    return SoldProductDto.builder().itemsSold(soldProduct.getItemsSold())
        .productId(soldProduct.getProductId()).build();
  }
}
