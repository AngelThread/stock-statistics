package com.stock.statistics.stockstatistics.controller;

import com.stock.statistics.stockstatistics.controller.dto.StockInfoDto;
import com.stock.statistics.stockstatistics.controller.dto.StockSummary;
import com.stock.statistics.stockstatistics.modal.StockData;
import java.time.LocalDateTime;

public class StockInfoDtoUtil {

  public static StockInfoDto toStockInfoDto(StockData stockData, LocalDateTime requestTime) {
    return StockInfoDto.builder().productId(stockData.getProductId()).requestTimestamp(requestTime)
        .stock(
            StockSummary.builder().id(stockData.getId()).quantity(stockData.getQuantity())
                .timestamp(stockData.getTimestamp()).build()).build();
  }

}
