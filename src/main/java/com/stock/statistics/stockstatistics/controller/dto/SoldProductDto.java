package com.stock.statistics.stockstatistics.controller.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
@JsonDeserialize(builder = SoldProductDto.SoldProductDtoBuilder.class)
public class SoldProductDto {


  private String productId;
  private int itemsSold;

  @JsonPOJOBuilder(withPrefix = "")
  public static class SoldProductDtoBuilder {

  }
}
