package com.stock.statistics.stockstatistics.controller.exception;

public class InvalidStockInformationSentException extends RuntimeException {

  public InvalidStockInformationSentException(String message) {
    super(message);
  }
}