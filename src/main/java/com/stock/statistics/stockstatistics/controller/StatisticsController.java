package com.stock.statistics.stockstatistics.controller;

import com.stock.statistics.stockstatistics.controller.dto.Range;
import com.stock.statistics.stockstatistics.controller.dto.StockStatisticsDto;
import com.stock.statistics.stockstatistics.controller.dto.StockStatisticsDtoUtil;
import com.stock.statistics.stockstatistics.modal.StockStatistics;
import com.stock.statistics.stockstatistics.service.StatisticsService;
import java.time.LocalDateTime;
import javax.validation.constraints.NotEmpty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class StatisticsController {

  @Autowired
  private StatisticsService statisticsService;

  @GetMapping(EndPoints.STATISTICS)
  @ResponseBody
  public ResponseEntity<Object> getStatistics(@RequestParam @NotEmpty Range time) {
    LocalDateTime requestTimestamp = LocalDateTime.now();
    StockStatistics stockStatistics = statisticsService.getStockStatistics(time);

    StockStatisticsDto stockStatisticsDto = StockStatisticsDtoUtil
        .toStockStatisticsDto(stockStatistics, time.getName(),requestTimestamp);

    return new ResponseEntity<Object>(stockStatisticsDto, HttpStatus.OK);
  }
}