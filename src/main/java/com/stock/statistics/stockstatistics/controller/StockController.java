package com.stock.statistics.stockstatistics.controller;

import static com.stock.statistics.stockstatistics.controller.CustomStockDtoValidator.checkIfQuantityInvalid;
import static com.stock.statistics.stockstatistics.controller.CustomStockDtoValidator.checkIfStockDtoTimeStampIsFuture;

import com.stock.statistics.stockstatistics.controller.dto.StockDto;
import com.stock.statistics.stockstatistics.controller.dto.StockInfoDto;
import com.stock.statistics.stockstatistics.controller.exception.InvalidStockInformationSentException;
import com.stock.statistics.stockstatistics.controller.exception.NotUniqueStockIdException;
import com.stock.statistics.stockstatistics.entity.Stock;
import com.stock.statistics.stockstatistics.modal.StockData;
import com.stock.statistics.stockstatistics.modal.StockDataUtil;
import com.stock.statistics.stockstatistics.service.StatisticsService;
import com.stock.statistics.stockstatistics.service.StockService;
import java.time.LocalDateTime;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class StockController {

  @Autowired
  private StockService stockService;


  @PostMapping(EndPoints.UPDATE_STOCK)
  @ResponseBody
  public ResponseEntity<Object> clientRequest(@Valid @RequestBody StockDto request) {
    validateRequest(request);
    try {
      stockService.save(StockDataUtil.toStockData(request));
    } catch (DataIntegrityViolationException exception) {
      throw new NotUniqueStockIdException(
          "There is already stockId in the system with stockId:" + request.getId());
    }
    return new ResponseEntity<>(request, HttpStatus.CREATED);
  }

  private void validateRequest(@Valid @RequestBody StockDto request) {
    if (checkIfStockDtoTimeStampIsFuture(request)) {
      throw new InvalidStockInformationSentException(String
          .format("Stock information with stock id:%s has future timestamp!",
              request.getTimestamp()));
    }

    if (checkIfQuantityInvalid(request)) {
      throw new InvalidStockInformationSentException(String
          .format("Stock information with stock id:%s has not valid quantity information!",
              request.getTimestamp()));
    }
  }

  @GetMapping(EndPoints.STOCK)
  @ResponseBody
  public ResponseEntity<Object> getStock(@RequestParam @NotEmpty String productId) {
    LocalDateTime requestTime = LocalDateTime.now();

    Optional<Stock> stockByProductId = stockService.findStockByProductId(productId);
    if (!stockByProductId.isPresent()) {
      return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
    StockData stockData = StockDataUtil.fromStocktoStockData(stockByProductId.get());
    StockInfoDto stockInfoDto = StockInfoDtoUtil.toStockInfoDto(stockData, requestTime);

    return new ResponseEntity<>(stockInfoDto, HttpStatus.OK);
  }

}
