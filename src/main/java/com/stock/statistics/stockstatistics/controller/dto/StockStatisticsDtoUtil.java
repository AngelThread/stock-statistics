package com.stock.statistics.stockstatistics.controller.dto;

import com.stock.statistics.stockstatistics.entity.SoldProduct;
import com.stock.statistics.stockstatistics.modal.StockDataUtil;
import com.stock.statistics.stockstatistics.modal.StockStatistics;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StockStatisticsDtoUtil {

  public static StockStatisticsDto toStockStatisticsDto(StockStatistics stockStatistics,
      String range, LocalDateTime requestTimestamp) {
    if (Objects.isNull(stockStatistics)) {
      return null;
    }

    List<SoldProduct> topSellingProducts = stockStatistics.getTopSellingProducts();
    List<SoldProductDto> soldProductDtos = topSellingProducts.stream().filter(Objects::nonNull)
        .map(SoldProductDtoUtil::toSoldProductDto).collect(
            Collectors.toList());

    List<StockDto> stockDtos = stockStatistics.getTopAvailableProducts().stream()
        .filter(Objects::nonNull).map(StockDataUtil::fromStocktoStockData)
        .map(StockDataUtil::toStockDto).collect(Collectors.toList());

    Collections.sort(topSellingProducts, (o1, o2) -> o2.getItemsSold() - o1.getItemsSold());

    Collections.sort(stockDtos, (o1, o2) -> o2.getQuantity() - o1.getQuantity());

    return StockStatisticsDto.builder().range(range).requestTimestamp(requestTimestamp)
        .topSellingProducts(soldProductDtos).topAvailableProducts(stockDtos).build();
  }

}
