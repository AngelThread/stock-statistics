package com.stock.statistics.stockstatistics.controller.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
@JsonDeserialize(builder = StockStatisticsDto.StockStatisticsDtoBuilder.class)
public class StockStatisticsDto {

  private LocalDateTime requestTimestamp;
  private String range;
  private List<StockDto> topAvailableProducts;
  private List<SoldProductDto> topSellingProducts;

  @JsonPOJOBuilder(withPrefix = "")
  public static class StockStatisticsDtoBuilder {

  }
}
