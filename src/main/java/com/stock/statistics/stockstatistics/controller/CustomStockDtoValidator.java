package com.stock.statistics.stockstatistics.controller;

import com.stock.statistics.stockstatistics.controller.dto.StockDto;
import java.time.LocalDateTime;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.RequestBody;

public class CustomStockDtoValidator {

  public static boolean checkIfStockDtoTimeStampIsFuture(@Valid @RequestBody StockDto request) {
    return request.getTimestamp().isAfter(LocalDateTime.now());
  }

  public static boolean checkIfQuantityInvalid(@Valid @RequestBody StockDto request) {
    return request.getQuantity() < 0;
  }

}
