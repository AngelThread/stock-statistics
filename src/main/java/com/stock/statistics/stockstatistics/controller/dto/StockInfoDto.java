package com.stock.statistics.stockstatistics.controller.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

@Builder
@Value
@JsonDeserialize(builder = StockInfoDto.StockInfoDtoBuilder.class)
public class StockInfoDto {

  private String productId;
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  private LocalDateTime requestTimestamp;
  private StockSummary stock;


  @JsonPOJOBuilder(withPrefix = "")
  public static class StockInfoDtoBuilder {

  }


}
