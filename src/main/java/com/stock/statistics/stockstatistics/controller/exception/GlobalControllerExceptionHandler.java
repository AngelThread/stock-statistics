package com.stock.statistics.stockstatistics.controller.exception;


import com.fasterxml.jackson.core.JsonParseException;
import com.stock.statistics.stockstatistics.controller.StockController;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(assignableTypes = {StockController.class})

@Slf4j
public class GlobalControllerExceptionHandler {


  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler({JsonParseException.class,
      MethodArgumentNotValidException.class, HttpMessageNotReadableException.class,
      NotUniqueStockIdException.class, InvalidStockInformationSentException.class
  })
  public ResponseEntity<Object> handleRequestsWithError(HttpServletRequest req, Exception ex) {

    return new ResponseEntity<>(buildErrorMessage(ex), HttpStatus.BAD_REQUEST);

  }

  @ResponseStatus(HttpStatus.NO_CONTENT)
  @ExceptionHandler({OutdatedStockException.class})
  public ResponseEntity<Object> handleOutdatedStockException(HttpServletRequest req, Exception ex) {

    return new ResponseEntity<>(buildErrorMessage(ex), HttpStatus.NO_CONTENT);

  }

  private ErrorMessage buildErrorMessage(Exception ex) {

    List<FieldError> fieldErrors = Collections.emptyList();
    List<ObjectError> globalErrors = Collections.emptyList();
    List<String> errors = new ArrayList<>();

    if (ex instanceof MethodArgumentNotValidException) {
      fieldErrors = ((MethodArgumentNotValidException) ex).getBindingResult().getFieldErrors();
      globalErrors = ((MethodArgumentNotValidException) ex).getBindingResult().getGlobalErrors();
    }

    if (ex instanceof NotUniqueStockIdException
        || ex instanceof InvalidStockInformationSentException) {
      errors.add("Invalid Request," + ex.getMessage());
      return new ErrorMessage(errors, HttpStatus.BAD_REQUEST.value());
    }
    if (ex instanceof OutdatedStockException) {
      errors.add("Invalid Request," + ex.getMessage());
      return new ErrorMessage(errors, HttpStatus.NO_CONTENT.value());
    }
    if (ex instanceof HttpMessageNotReadableException) {
      errors.add("Http Message Not Readable," + "not valid parsable body!");
    }

    errors = convertValidationErrorsToErrorMessage(fieldErrors, globalErrors, errors);
    return new ErrorMessage(errors, HttpStatus.BAD_REQUEST.value());

  }

  private List<String> convertValidationErrorsToErrorMessage(List<FieldError> fieldErrors,
      List<ObjectError> globalErrors, List<String> errors) {
    String error;
    for (FieldError fieldError : fieldErrors) {
      error = fieldError.getField() + ", " + fieldError.getDefaultMessage();
      errors.add(error);
    }
    for (ObjectError objectError : globalErrors) {
      error = objectError.getObjectName() + ", " + objectError.getDefaultMessage();
      errors.add(error);
    }
    return errors;
  }

}
