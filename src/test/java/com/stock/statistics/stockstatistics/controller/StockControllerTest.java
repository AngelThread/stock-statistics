package com.stock.statistics.stockstatistics.controller;

import static com.stock.statistics.stockstatistics.controller.EndPoints.STATISTICS;
import static com.stock.statistics.stockstatistics.controller.EndPoints.STOCK;
import static com.stock.statistics.stockstatistics.controller.EndPoints.UPDATE_STOCK;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stock.statistics.stockstatistics.controller.dto.StockDto;
import com.stock.statistics.stockstatistics.controller.exception.ErrorMessage;
import com.stock.statistics.stockstatistics.entity.Stock;
import com.stock.statistics.stockstatistics.modal.StockData;
import com.stock.statistics.stockstatistics.service.StockService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@WebMvcTest(StockController.class)
@RunWith(SpringRunner.class)
public class StockControllerTest {

  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private StockService stockService;

  final ObjectMapper objectMapper = new ObjectMapper();

  @Test
  public void updateStockRequestWithNullFields() throws Exception {
    ErrorMessage errorMessage = prepareExpectedErrorMessage();

    StockDto stockDto = StockDto.builder().build();
    MvcResult mvcResult = mockMvc.perform(post(UPDATE_STOCK)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(stockDto)))
        .andExpect(status().is(400))
        .andReturn();

    String contentAsString = mvcResult.getResponse().getContentAsString();
    ErrorMessage errorMessageResponse = objectMapper.readValue(contentAsString, ErrorMessage.class);
    java.util.Collections.sort(errorMessageResponse.getErrors());

    Assert.assertTrue(Objects.equals(errorMessageResponse, errorMessage));
  }

  private ErrorMessage prepareExpectedErrorMessage() {
    String e1 = "productId, must not be empty";
    String e2 = "id, must not be empty";

    List<String> errors = new ArrayList<>();
    errors.add(e1);
    errors.add(e2);
    java.util.Collections.sort(errors);
    return new ErrorMessage(errors, 400);
  }

  @Test
  public void updateStockRequestSuccessful() throws Exception {

    StockDto stockDto = StockDto.builder().id("1").productId("product1").quantity(1).timestamp(
        LocalDateTime.now()).build();
    mockMvc.perform(post(UPDATE_STOCK)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(stockDto)))
        .andExpect(status().is(201));

    verify(stockService, times(1)).save(any(StockData.class));

  }

  @Test
  public void updateStockRequestWithFutureTimestamp() throws Exception {

    StockDto stockDto = StockDto.builder().id("1").productId("product1").quantity(1).timestamp(
        LocalDateTime.now().plusDays(2)).build();
    mockMvc.perform(post(UPDATE_STOCK)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(stockDto)))
        .andExpect(status().is(400));

  }

  @Test
  public void requestSuccessful() throws Exception {

    String productId = "vegetable-123";
    when(stockService.findStockByProductId(eq(productId))).thenReturn(
        java.util.Optional.of(new Stock()));

    mockMvc.perform(get(STOCK).param("productId", productId)).andExpect(status().is(200));

    verify(stockService, times(1)).findStockByProductId(eq(productId));

  }

}