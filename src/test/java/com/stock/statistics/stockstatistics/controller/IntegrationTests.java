package com.stock.statistics.stockstatistics.controller;

import static com.stock.statistics.stockstatistics.controller.EndPoints.STATISTICS;
import static com.stock.statistics.stockstatistics.controller.EndPoints.STOCK;
import static com.stock.statistics.stockstatistics.controller.EndPoints.UPDATE_STOCK;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.stock.statistics.stockstatistics.controller.dto.Range;
import com.stock.statistics.stockstatistics.controller.dto.StockDto;
import com.stock.statistics.stockstatistics.controller.dto.StockInfoDto;
import com.stock.statistics.stockstatistics.controller.dto.StockStatisticsDto;
import com.stock.statistics.stockstatistics.repository.SoldProductRepository;
import com.stock.statistics.stockstatistics.repository.StockRepository;
import com.stock.statistics.stockstatistics.service.StatisticsService;
import com.stock.statistics.stockstatistics.service.StockService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.function.Function;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
@TestPropertySource(
    locations = "classpath:application-integrationtest.properties")
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class IntegrationTests {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private StockService stockService;
  @Autowired
  private StatisticsService statisticsService;
  @Autowired
  private StockRepository repository;
  @Autowired
  private SoldProductRepository soldProductRepository;

  final ObjectMapper objectMapper = new ObjectMapper();


  @Before
  public void cleanUp() {
    repository.deleteAll();
    soldProductRepository.deleteAll();
  }


  @Test
  public void updateStockRequestWithTheSameStockId() throws Exception {

    StockDto stockDto = StockDto.builder().id("1").productId("product1").quantity(1).timestamp(
        LocalDateTime.now().minusDays(3)).build();
    sentUpdatePostRequestAndCheckStatus(stockDto);

    StockDto stockDto2 = StockDto.builder().id("1").productId("product2").quantity(1).timestamp(
        LocalDateTime.now().minusDays(2)).build();
    mockMvc.perform(post(UPDATE_STOCK)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(stockDto2)))
        .andExpect(status().is(400));

  }


  @Test
  public void updateStockRequestWithOlderTimestampField() throws Exception {

    StockDto stockDto = StockDto.builder().id("1").productId("product1").quantity(1).timestamp(
        LocalDateTime.now()).build();
    sentUpdatePostRequestAndCheckStatus(stockDto);

    StockDto stockDto2 = StockDto.builder().id("2").productId("product1").quantity(1).timestamp(
        LocalDateTime.now().minusDays(1)).build();
    mockMvc.perform(post(UPDATE_STOCK)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(stockDto2)))
        .andExpect(status().is(204));
  }

  @Test
  public void updateStockRequestWithAndGetWithProductId() throws Exception {
    objectMapper.registerModule(new JavaTimeModule());

    String productId = "product1";
    LocalDateTime timestamp = LocalDateTime.now();
    String stockId = "1";
    int quantity = 1;
    StockDto stockDto = StockDto.builder().id(stockId).productId(productId).quantity(quantity)
        .timestamp(
            timestamp).build();
    sentUpdatePostRequestAndCheckStatus(stockDto);

    MvcResult mvcResult = mockMvc.perform(get(STOCK)
        .contentType(MediaType.APPLICATION_JSON)
        .param("productId", productId))
        .andExpect(status().is(200)).andReturn();

    String contentAsString = mvcResult.getResponse().getContentAsString();
    StockInfoDto stockInfoDto = objectMapper.readValue(contentAsString, StockInfoDto.class);

    Assert.assertEquals(productId, stockInfoDto.getProductId());
    Assert.assertEquals(timestamp, stockInfoDto.getStock().getTimestamp());
    Assert.assertEquals(stockId, stockInfoDto.getStock().getId());
    Assert.assertEquals(quantity, stockInfoDto.getStock().getQuantity());

  }

  @Test
  public void updateStockRequestWithAndGetStatisticsToCompareTopAvailableProducts()
      throws Exception {
    objectMapper.registerModule(new JavaTimeModule());

    for (int i = 1; i < 5; i++) {
      StockDto stockDto = getStockDto("product" + i, LocalDateTime.now(), "1" + i, i);

      sentUpdatePostRequestAndCheckStatus(stockDto);
    }

    StockStatisticsDto stockInfoDto = callStatisticsAndGetStockStatisticsDto(Range.today);

    Assert.assertEquals(Range.today.getName(), stockInfoDto.getRange());
    Assert.assertEquals(Boolean.TRUE,
        stockInfoDto.getRequestTimestamp().isBefore(LocalDateTime.now()));

    Assert.assertEquals(4, stockInfoDto.getTopAvailableProducts().get(0).getQuantity());
    Assert.assertEquals(3, stockInfoDto.getTopAvailableProducts().get(1).getQuantity());
    Assert.assertEquals(2, stockInfoDto.getTopAvailableProducts().get(2).getQuantity());

    Assert.assertEquals(new ArrayList<>(), stockInfoDto.getTopSellingProducts());

  }

  @Test
  public void updateStockRequestWithAndGetStatisticsToCompareTopSellingProducts()
      throws Exception {
    objectMapper.registerModule(new JavaTimeModule());

    createNumberOfProductWithQuantity(1, "productOld", "ex", LocalDateTime.now().minusDays(1), i -> 10000);
    createNumberOfProductWithQuantity(1, "productOld", "ex", LocalDateTime.now().minusDays(1), i -> 5000);

    createNumberOfProductWithQuantity(4, "product", "1", LocalDateTime.now(), i -> 100);
    createNumberOfProductWithQuantity(3, "product", "2", LocalDateTime.now(),
        i -> (100 - (i * 10)));

    StockStatisticsDto stockInfoDto = callStatisticsAndGetStockStatisticsDto(Range.today);

    Assert.assertEquals(Range.today.getName(), stockInfoDto.getRange());
    Assert.assertEquals(Boolean.TRUE,
        stockInfoDto.getRequestTimestamp().isBefore(LocalDateTime.now()));

    Assert.assertEquals(100, stockInfoDto.getTopAvailableProducts().get(0).getQuantity());
    Assert.assertEquals(90, stockInfoDto.getTopAvailableProducts().get(1).getQuantity());
    Assert.assertEquals(80, stockInfoDto.getTopAvailableProducts().get(2).getQuantity());

    Assert.assertEquals(30, stockInfoDto.getTopSellingProducts().get(0).getItemsSold());
    Assert.assertEquals(20, stockInfoDto.getTopSellingProducts().get(1).getItemsSold());
    Assert.assertEquals(10, stockInfoDto.getTopSellingProducts().get(2).getItemsSold());

  }

  public StockStatisticsDto callStatisticsAndGetStockStatisticsDto(Range range) throws Exception {
    MvcResult mvcResult = callStatistics(range);

    String contentAsString = mvcResult.getResponse().getContentAsString();
    return objectMapper
        .readValue(contentAsString, StockStatisticsDto.class);
  }

  public MvcResult callStatistics(Range today) throws Exception {
    return mockMvc.perform(get(STATISTICS)
        .contentType(MediaType.APPLICATION_JSON).param("time", today.getName()))
        .andExpect(status().is(200)).andReturn();
  }

  public void sentUpdatePostRequestAndCheckStatus(StockDto stockDto) throws Exception {
    mockMvc.perform(post(UPDATE_STOCK)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(stockDto)))
        .andExpect(status().is(201));
  }

  @Test
  public void updateStockRequestWithAndGetMonthlyStatisticsToCompareTopSellingProducts()
      throws Exception {
    objectMapper.registerModule(new JavaTimeModule());

    createNumberOfProductWithQuantity(5, "product", "1", LocalDateTime.now(), i -> 100);

    createNumberOfProductWithQuantity(2, "productMax", "2", LocalDateTime.now(), i -> 5000);

    createNumberOfProductWithQuantity(1, "productMax", "3", LocalDateTime.now(), i -> i * 1000);

    createNumberOfProductWithQuantity(3, "product", "4", LocalDateTime.now(),
        i -> (100 - (i * 10)));

    createNumberOfProductWithQuantity(1, "productOld", "ex", LocalDateTime.now().minusMonths(1).minusDays(1), i -> 50000);
    createNumberOfProductWithQuantity(1, "productOld", "ex", LocalDateTime.now().minusMonths(1).minusDays(1), i -> 30000);


    StockStatisticsDto stockInfoDto = callStatisticsAndGetStockStatisticsDto(Range.lastMonth);

    Assert.assertEquals(Range.lastMonth.getName(), stockInfoDto.getRange());
    Assert.assertEquals(Boolean.TRUE,
        stockInfoDto.getRequestTimestamp().isBefore(LocalDateTime.now()));

    Assert.assertEquals(5000, stockInfoDto.getTopAvailableProducts().get(0).getQuantity());
    Assert.assertEquals(1000, stockInfoDto.getTopAvailableProducts().get(1).getQuantity());
    Assert.assertEquals(100, stockInfoDto.getTopAvailableProducts().get(2).getQuantity());

    Assert.assertEquals(4000, stockInfoDto.getTopSellingProducts().get(0).getItemsSold());
    Assert.assertEquals(30, stockInfoDto.getTopSellingProducts().get(1).getItemsSold());
    Assert.assertEquals(20, stockInfoDto.getTopSellingProducts().get(2).getItemsSold());

  }

  public StockStatisticsDto getStockStatisticsDto(MvcResult mvcResult) throws java.io.IOException {
    String contentAsString = mvcResult.getResponse().getContentAsString();
    return objectMapper
        .readValue(contentAsString, StockStatisticsDto.class);
  }

  public void createNumberOfProductWithQuantity(int numberOfProduct,
      String productNameBase, String stockIdBase, LocalDateTime timestamp,
      Function<Integer, Integer> calculateQuantity)
      throws Exception {
    for (int i = 1; i < numberOfProduct + 1; i++) {
      postNewStockInfo(productNameBase + i, timestamp, stockIdBase + i,
          calculateQuantity.apply(i));
    }
  }

  public void postNewStockInfo(String productId, LocalDateTime timestamp, String stockId,
      int quantity) throws Exception {
    StockDto stockDto = getStockDto(productId, timestamp, stockId, quantity);

    sentUpdatePostRequestAndCheckStatus(stockDto);
  }

  public StockDto getStockDto(String productId, LocalDateTime timestamp, String stockId,
      int quantity) {
    return StockDto.builder().id(stockId).productId(productId).quantity(quantity)
        .timestamp(
            timestamp).build();
  }
}